<?php

use Illuminate\Database\Seeder;
use App\PaymentMode;

class PaymentModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMode::insert([
        	['name' => 'Paypal'],
        	['name' => 'MasterCard'],
        	['name' => 'Visa']
        ]);
    }
}
