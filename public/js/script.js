const alert_container = document.querySelector("#alert_container");

//badge
//cartBody


function removeAlert()
{
	setTimeout(function(){
		alert_container.innerHTML = "";
	//3000 milliseconds or 3 seconds
	}, 3000);
}

function displaySuccessMessage(content)
{
	let alert_success = document.createElement("div");
	alert_success.setAttribute("class", "alert alert-success");
	alert_success.setAttribute("id", "alert_success");
	alert_success.textContent = content;
	// let alert_container = document.querySelector("#alert_container");
	alert_container.append(alert_success);
	removeAlert();
}

function displayErrorMessage(content)
{
	//<div class="alert alert-danger" id="alert_error">
	let alert_error = document.createElement("div");
	alert_error.setAttribute("class", "alert alert-danger");
	alert_error.setAttribute("id", "alert_error");
	alert_error.textContent = content;
	// let alert_container = document.querySelector("#alert_container");
	alert_container.append(alert_error);
	removeAlert();
}

//not yet working; refactor to fetch
function updateBadge(operator, value)
{
	let badge = document.querySelector("#cart-count");
	//1) select all input fields with a class name of update-quantity
	//let count = document.querySelectorAll(".update-quantity");

	//2) convert the nodelist into (using spread [...])
	//let count = [...document.querySelectorAll(".update-quantity")];

	//3) iterate through each element to get the value and store results in another array
	//let count = [...document.querySelectorAll(".update-quantity")].map(element => [element.valueAsNumber]);	
	//let count = [...document.querySelectorAll(".update-quantity")].flatMap(element => [element.valueAsNumber]);	

	//4) get the sum of the values 
	let count = [...document.querySelectorAll(".update-quantity")].flatMap(element => [element.valueAsNumber]).reduce((accumulator, currentValue) => accumulator + currentValue);

	//console.log(count);
	//return count;
	badge.innerHTML = count;
}

//prevent default submission of input fields
document.addEventListener("submit", function(e){

	let flag = false;

	if(e.target.classList.contains("update-quantity-form"))
	{
		flag = true;
	}

	if(e.target.classList.contains("addToCart-form"))
	{
		flag = true;
	}

	if(flag)
	{
		e.preventDefault();
	}
})

document.addEventListener("click", function(e) 
{
	// console.log(e.target.classList.contains("btn-delete-product"));
	//====DELETE PRODUCT
	if(e.target.classList.contains("btn-delete-product")) 
	{
		// alert("hello");
		let button = e.target;
		//get product_id
		let product_id = button.dataset.id;
		//get product_name
		let product_name = button.dataset.name;
		//put product_name inside span
		let modal_text = document.querySelector("#delete_product_name");
		modal_text.innerHTML = product_name;
		//get the modal's form
		let modal_form = document.querySelector("#delete_modal_form");
		//set action attribute to modal form
		modal_form.setAttribute("action", `/products/${product_id}`);
	}

	//=======ADD TO CART
	if(e.target.classList.contains("btn-addToCart"))
	{
		//alert("test");
		let button = e.target;
		// console.log(button);
		if(button.previousElementSibling.children[0].value >= 1)
		{
			let product_name = button.dataset.name;
			let product_id = button.dataset.id;

			//let form = document.querySelector("#addToCart-form");
			//let form = document.querySelector(`form[class="form-${product_id}"]`);
			let form = document.querySelector(`#form-${product_id}`);

			fetch(`/addToCart/${product_id}`, {
				method: "POST",
				credentials: "same-origin",
				body: new FormData(form)
			})
				.then(function(response){
					return response.text();
				})
				.then(function(data_from_fetch){
					// console.log(data_from_fetch);
					//update badge count
					let badge = document.querySelector("#cart-count");
					badge.innerHTML = data_from_fetch;

					//display success message
					let content = `${product_name} has been added to cart!`;
					displaySuccessMessage(content);

				})
				.catch(function(error){
					console.log("Request failed", error);
				})
		}
		else 
		{
			let content = "Please add a valid quantity";
			displayErrorMessage(content);
		}
	}

	// ======== TRANSFER PRODUCT TO BE DELETE TO MODAL
	if(e.target.classList.contains("btn-show-delete-modal"))
	{
		// alert("test");
		let button = e.target;
		let product_name = button.dataset.name;
		let product_id = button.dataset.id;

		let modal_form = document.querySelector("#delete_cart_product_form");
		let modal_button = modal_form.lastElementChild;
		// console.log(modal_button);
		modal_button.setAttribute("data-id", product_id);
		modal_button.setAttribute("data-name", product_name);

		let modal_text = document.querySelector("#delete_cart_product_name");
		modal_text.innerHTML = product_name;
	}	

	//============ DELETE CART PRODUCT BUTTON
	if(e.target.classList.contains("delete_cart_product_button"))
	{
		let delete_cart_product = document.querySelector("#delete_cart_product_button");
		let product_id = delete_cart_product.dataset.id;
		let product_name = delete_cart_product.dataset.name;

		//update badge
		// let badge = document.querySelector("#cart-count");
		// let badge_current_value = parseInt(badge.innerHTML);
		// //get value of item to be deleted
		// let inputField = document.querySelector(`input[data-id="${product_id}"]`);

		// let deleted_product_value = inputField.value;
		// let badge_new_value = badge_current_value - deleted_product_value;

		//token
		let token = document
		.querySelector("meta[name='csrf-token']")
		.getAttribute("content");

		let data = new FormData();
		data.append("_method", "DELETE");

		fetch(`/destroyCartProduct/${product_id}`, 
		{
			method: "POST",
			credentials: "same-origin",
			body: data,
			headers: {
				"X-CSRF-TOKEN": token
			}
		})
			.then(function(response){
				return response.text();
			})
			.then(function(data_from_fetch){
				// console.log(data_from_fetch);
				// alert("test");

				let cartBody = document.querySelector("#cart-body");
				cartBody.innerHTML = data_from_fetch;

				//close modal
				$("#delete_cart_product_modal").modal("toggle");

				//update badge
				//badge.innerHTML = badge_new_value;
				updateBadge();
			})
			.catch(function(error){
				console.log("Request failed", error);
			})
	}
})

document.addEventListener("change", function(e) 
{
	//refactor this (e.keyCode)
	//e.preventDefault(); 

	// ===== UPDATE QUANTITY
	if(e.target.classList.contains("update-quantity"))
	{
		let input = e.target;
		let new_quantity = input.value;
		let product_id = input.dataset.id;
		let product_name = input.dataset.name;
		let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");

		let data = new FormData();
		data.append("product_id", product_id);
		data.append("new_quantity", new_quantity);
		data.append("_method", "PUT");

		fetch(`/updateQuantity/${product_id}`,
		{
			method: "POST",
			credentials: "same-origin",
			body: data,
			headers: {
				"X-CSRF-TOKEN": token
			}
		})
			.then(function(response){
				return response.text();
			})
			.then(function(data_from_fetch){
				// console.log(data_from_fetch);
				let tableBody = document.querySelector("#cart-body");
				tableBody.innerHTML = data_from_fetch;
				let content = `${product_name} has been updated!`;
				//new_quantity should be the difference between old_quantity and new_quantity
				
				updateBadge();
				displaySuccessMessage(content);
			})
			.catch(function(error){
				console.log("Request failed", error);
			})
	}
})


//============== SEARCH
let inputSearch = document.querySelector("#search");
inputSearch.addEventListener("keydown", function(e){
	console.log(e.keyCode);
	if(e.keyCode == 13) {
		e.preventDefault();
		let searchKey = inputSearch.value;
		// alert(searchKey);

		let form = document.querySelector("#search-form");

		fetch(`/search`, {
			method: "POST",
			body: new FormData(form)
		})
		.then(function(response){
			return response.text();
		})
		.then(function(data_from_fetch){
			if(data_from_fetch != "none"){
				let container = document.querySelector("#main-container");
				container.innerHTML = data_from_fetch;
			} else {
				let content = `Sorry ${searchKey} didn't match any of our products. :( `;
				displayErrorMessage(content);
			}
		})
		.catch(function(error){
			console.log("Request failed", error);
		})
	}
})
