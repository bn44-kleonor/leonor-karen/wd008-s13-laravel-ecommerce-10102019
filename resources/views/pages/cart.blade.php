@extends("layouts.app")
@section("content")
<!-- CART PAGE -->
<div class="row">
	<div class="col">
		<div class="display-4">
			{{ $title }}
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<table class="table">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col"></th>
		      <th scope="col">Name</th>
		      <th scope="col">Price</th>
		      <th scope="col">Quantity</th>
		      <th scope="col">Subtotal</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody id="cart-body">
		  	@if(Session::has('cart'))
		  		<!-- PRODUCTS -->
		  		@foreach($cart_products as $product)
			    <tr>
			      <th scope="row">
			      	<button 
			      	type="button" 
			      	class="btn btn-flat text-secondary btn-show-delete-modal" 
			      	data-id="{{ $product->id }}" 
			      	data-name="{{ $product->name }}"
			      	data-toggle="modal"
			      	data-target="#delete_cart_product_modal"
			      	>
			      		X
			      	</button>
			      </th>
			      <td>{{ $product->name }}</td>
			      <td>&#36; {{ $product->price }}</td>
			      <td>
			      	<!-- UPDATE QUANTITY -->
			      	<form method="POST" id="update-quantity-form-{{ $product->id}}">
			      		@csrf
			      		{{ method_field("PUT") }}
				      	<div class="form-group">
				      		<input type="number" 
				      		class="form-control update-quantity" 
				      		min="1" value="{{ $product->quantity }}" 
				      		data-id="{{ $product->id }}" 
				      		data-name="{{ $product->name }}">
				      	</div>
				    </form>
			      </td>
			      <td>&#36; {{ $product->subtotal }}</td>
			      <td>
			      	<a href="#" class="btn btn-success">View</a>
			      </td>
			    </tr>
		  		@endforeach
		  	@endif
			<!-- TOTAL -->
				<tr>
					<td colspan="4" class="text-right">
						<h4>Total</h4>
					</td>
					<td colspan="4" class="text-right">
						<!-- variable from PageController.cart -->
						<h4>&#36; {{ $total }}</h4>
					</td>
				</tr>
		  </tbody>
		</table>		
	</div>
</div>
<div class="row">
    <div class="col d-flex">
        <a href="{{ URL::previous() }}" class='btn btn-secondary'>Back</a>
        <a href="/checkout" class='btn btn-primary ml-auto'>Checkout</a>
    </div>
</div>

<!-- MODAL -->
<!-- Modal -->
<div class="modal fade" id="delete_cart_product_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Delete Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Do you want to delete <span id="delete_cart_product_name"></span> from your cart?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <form method="POST" id="delete_cart_product_form">
        	@csrf
        	{{ method_field("DELETE") }}
        	<button 
        	type="button" 
        	class="btn btn-danger delete_cart_product_button" 
        	id="delete_cart_product_button">
        		Delete Product
        	</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection