@extends("layouts.app")
@section("content")

<div class="row">
	<div class="col">
		<div class="display-4">{{ $title }}</div>
	</div>
</div>

<form method="POST" enctype="multipart/form-data" action="/products/{{ $product->id }}">
	@csrf
	{{ method_field("PUT") }}
	<div class="row">
		<div class="col-6">
			<img src="{{ $product->image }}" class="card-img-top mb-3">
			<!-- FILE -->
			<div class="form-group">
				<div class="custom-file">
				  <input type="file" class="custom-file-input" id="image" name="image">
				  <label class="custom-file-label" for="customFile">Choose file</label>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<!-- Name -->
					<div class="form-group">
						<label for="name">
							Name:
						</label>
						<input type="text" name="name" id="name" value="{{ $product->name }}" class="form-control">
					</div>
					<!-- Price -->
					<div class="form-group">
						<label for="price">
							Price:
						</label>
						<input type="text" name="price" id="price" value="{{ $product->price }}" step="0.01" min="1" class="form-control">
					</div>
					<!-- Description -->
					<div class="form-group">
						<label for="description">
							Description:
						</label>
						<textarea id="description" name="description" class="form-control">{{ $product->description }}</textarea>
					</div>
					<!-- Stock -->
					<div class="form-group">
						<label for="stock">
							Stock:
						</label>
						<input type="number" name="stock" id="stock" value="{{ $product->stock }}" class="form-control">
					</div>
					<!-- Categories -->
					<div class="form-group">
						<label for="category">
							Category:
						</label>
						<select class="custom-select" name="category" id="category">
							@foreach($categories as $category)
								<option value="{{ $category->id }}"
									{{ $category->id == $product->category_id ? "selected" : "" }}
									>
									{{ $category->name }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-warning">
						Save Changes
					</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection