<div class="row mb-4">
    <div class="col">
        <div class="display-4">
            {{ $title }}
        </div>
    </div>
</div>
<div class="row mb-4">
    <div class="col">
        <p>Your transaction code is <span class="font-weight-bold">{{ $transaction_code }}</span>.</p>
    </div>
</div>
<div class="row mb-4">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title font-weight-bold">Order</h5>
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" width="20%">Name</th>
                            <th scope="col" width="20%">Price</th>
                            <th scope="col" width="20%">Quantity</th>
                            <th scope="col" width="20%">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                      {{-- dd($order) --}}
                      @foreach($order->products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>&#36; {{ $product->price }}</td>
                            <td>{{ $product->pivot->quantity }}</td>
                            <td>&#36; {{ $product->pivot->quantity * $product->price }}</td>
                        </tr>
                       @endforeach
                        <tr>
                            <td colspan="3" class="text-right">
                                <h4>{{ $total }}</h4>
                            </td>
                            <td class="text-left">
                                <h4>&#36; total </h4>
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <a href="#" class="btn btn-primary">View Transactions</a>
    </div>
</div>