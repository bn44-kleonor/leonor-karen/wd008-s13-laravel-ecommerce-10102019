        <!-- NAVBAR -->
        <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
         
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <form method= "POST" class="input-group input-group-sm my-2 my-lg-0" id="search-form">
                        @csrf
                      <input class="form-control mr-sm-2 " type="search" placeholder="Search Products" aria-label="Search" name="search" id="search">
                    </form>
         
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- CART -->
                        @if(!Auth::check() || Auth::user()->role == "user")
                            <li class="nav-item">
                                <a class="nav-link d-flex flex-row align-items-center" href="/cart">
                                    <span>Cart</span>
                                    <span class="badge badge-pill badge-light ml-1" id="cart-count">
                                       @if(Session::has("cart"))
                                            {{ collect(Session::get("cart"))->sum() }}
                                       @endif
                                    </span>
                                </a>
                            </li>
                        @endif
         
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>
         
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
         
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @if(Auth::check() && Auth::user()->role == 'admin')
                                <li class="nav-item">
                                    <a class="nav-link" href="/products">Products</a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="/cart">
                                        Cart
                                        <span class="badge badge-dark" id="cart-count">
                                            @if(Session::has("cart"))
                                                <!-- {{ collect(Session::has("cart"))->sum() }} -->
                                                <!-- {{ collect(Session::get("cart")) }} -->
                                                {{ collect(Session::get("cart"))->sum() }}
                                            @endif
                                        </span>
                                    </a>
                                </li>
                            @endif
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

