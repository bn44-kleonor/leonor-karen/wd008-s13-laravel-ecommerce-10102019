<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //a Status "belongs to" many Order (MANY-to-one)
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
